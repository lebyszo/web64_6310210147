const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/bmi', (req, res) => {
  let weight = parseFloat(req.query.weight)
  let height = parseFloat(req.query.height)
  var result = {}

  if( !isNaN(weight) && !isNaN(height) ){
    let bmi = weight / (height * height)

    result = {
        "status" : 200,
        "bmi"    : bmi
    }
}else{
    result = {
        "status" : 400,
        "message" : "Weight or Height is not number"
    }
}
  res.send(JSON.stringify(result))
})

app.get('/hello', (req, res) => {
    res.send('Hello '+req.query.name)
})

app.get('/triangle', (req, res) => {
    let height = parseFloat(req.query.height)
    let base = parseFloat(req.query.base)
    var result = {}

    if(!isNaN(height) && !isNaN(base)) {
        let area = ( 1 * base * height )/2

        result = {
            "status" : 200,
            "area"   : area 
        }
    }else{
        result = {
            "status" : 400,
            "message": "Height or Base is not number"
        }
    }
    res.send(JSON.stringify(result))
})

app.post('/score', (req, res) => {
    let name = req.query.name
    let score = parseInt(req.query.score)
    var result = {}

    if(!isNaN(score) && score <= 100) {
        let grade = "Default"
        if(score >= 80)
            grade = "A"
        else if(score >= 70)
            grade = "B"
        else if(score >= 60)
            grade = "C"
        else if(score >= 50)
            grade = "D"
        else
            grade = "E"

        result = {
            "status" : 200,
            "grade"  : grade
        }
    }else{
        result = {
            "status"  : 400,
            "message" : "Score is not number or over than 100"
        }
    }
    res.send(JSON.stringify(result))
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})