import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function AboutUs(props) {

    return(
        <Box sx={{width: "60%"}}>
            <Paper elevation={3}>
                <h2>จัดทำโดย : {props.name}</h2>
                <h3>ติดต่อ : {props.address} </h3>
                <h3>บ้าน : {props.province}</h3>
            </Paper>
        </Box>
    );
}

export default AboutUs;