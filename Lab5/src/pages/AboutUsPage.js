import AboutUs from "../components/AboutUs";

function AboutUsPage() {

    return(
        <div>
            <div align="center">
                <h2>คณะผู้จัดทำเว็บนี้</h2>
                <AboutUs name="Pond Tirapong"
                      address="6310210147@psu.ac.th"
                      province="สุราษฎร์ธานี" />

                <AboutUs name="Pong Tirapond"
                      address="6310210147@psu.ac.th"
                      province="สุราษฎร์ธานี" />

                <AboutUs name="Tong Pirapond"
                      address="6310210147@psu.ac.th"
                      province="สุราษฎร์ธานี" />
            </div>
        </div>
    );
}

export default AboutUsPage;