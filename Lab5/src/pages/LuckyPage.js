import LuckyNumber from "../components/LuckyNumber";
import { useState} from "react";

function LuckyPage() {

    const [ number , setNumber ] = useState("");
    const [ result , setResult ] = useState("");

    function calculateLucky() {
        
        let num = parseInt(number);
        if(num == 69)
        {
            setResult("ถูกแล้วจ้าา");
        }
        else
        {
            setResult("ผิดจ้าา");
        }
    }

    return(
        <div align="left">
            <div align="center">
                    ทายเลข Lucky Number กันดีกว่า
                <hr />
                    กรุณาทายเลขที่ต้องการ ( 0 - 99 ) : 
                    <input type="text"
                            value={number}
                            onChange={(e) => {setNumber(e.target.value);}} /><br />
                    <button onClick={() => calculateLucky() }> ทาย </button>

                { result != 0 &&
                    <div>
                        <hr />
                        
                        ผลลัพธ์
                        <LuckyNumber 
                            Result = { result }/>
                    </div>
                } 
            </div>
        </div>
    );
}

export default LuckyPage;