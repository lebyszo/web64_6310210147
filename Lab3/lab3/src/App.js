import logo from './logo.svg';
import './App.css';
import Header from './components/Header'
import Footer from './components/Footer';
import Body from './components/Body';
import Youtube from './components/Youtube';

function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Youtube />
      <Footer />
    </div>
  );
}

export default App;
