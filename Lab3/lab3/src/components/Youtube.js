import './Youtube.css'

function Youtube() {

    return(
        <div id="YoutubeBG">
        <h3>ฟังเพลงเพลิน ๆ กับพี่ป้าง</h3>
        <iframe id="video" width="560" height="315" src="https://www.youtube.com/embed/feTF2qrQ69I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    );
}

export default Youtube;