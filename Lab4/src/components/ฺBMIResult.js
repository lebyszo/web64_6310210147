import './BMIResult.css';

function BMIResult(props) {

    return(
        <div>
            <h3 class="A">คุณ : {props.name}</h3>
            <h3 class="B">มี BMI : {props.bmi}</h3>
            <h3>แปลว่า : <h1 class="C">{props.result}</h1></h3>
        </div>
    );
}

export default BMIResult;